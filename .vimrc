execute pathogen#infect()
syntax on
filetype plugin indent on
set number 
set tabstop=4
set softtabstop=4
set expandtab
set showmatch
set incsearch
set hlsearch
set backspace=indent,eol,start
colorscheme molokai
set laststatus=2
set statusline+=%f\ \ \ \   
set statusline+=%l/%L\ \ \ \ 
set statusline+=%c\ \ \ \ 
set mouse+=a
"NerdTree mappings and settings
map <C-N> :NERDTreeToggle<CR> 
"Toggle NerdTree with Ctrl-N
"
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"Quit vim if NerdTree is the only thing open

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
"Arrow keys for navigating dirs


"Syntastic settings
let mapleader = '-'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0 
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checker_args = '--ignore=E225'

command Sd SyntasticToggleMode
command S SyntasticCheck
"NerdCommenter Settings

"Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

"Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

"Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

"Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

"Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } }

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1
